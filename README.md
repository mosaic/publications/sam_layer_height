# SAM Layer Height
## Python Notebook to estimate cellular heights in shoot apical meristem images

----

#### Authors :
Guillaume Cerutti (Laboratoire RDP, ENS Lyon)

### Download

----

```bash
git clone https://gitlab.inria.fr/mosaic/publications/sam_layer_height.git
```

### Pre-requisite: install `conda`

----

If `conda` is not already installed (open a terminal window and type `conda` to check), you may choose to install either [the miniconda tool](https://docs.conda.io/en/latest/miniconda.html) or [the anaconda distribution](https://docs.anaconda.com/anaconda/install/) suitable for your OS.


### Create the conda environment

----

Use the provided `YAML` file to set up a `conda` environment containing all the necessary dependencies.

```bash
cd sam_layer_height
conda env create -f environment.yml
conda activate sam_layer_height
```

### Launch Jupyter notebook

----

You can launch the [`Jupyter`](https://jupyter.org/) notebook server by typing the following command:

```bash
jupyter notebook
```

In the `jupyter` window, you can now select the `notebook/` folder and double-click on the `sam_layer_height.ipynb` to launch the notebook

**Note**: everytime you want to use the notebook you need to activate the `conda` environment first by typing:

```bash
conda activate sam_layer_height
```

You can execute the first *Import* cells of the notebook to check that all the dependencies have been correctly installed.

### Configure the notebook parameters

----

The notebook is designed process any microscopy image of a shoot apical meristem provided as a `.tif` or a [`.czi`](https://www.zeiss.com/microscopy/en/products/software/zeiss-zen/czi-image-file-format.html) image file.

You will need to modify the first notebook cells to specify information about your image, as well as parameter values for the processing.

The most important fields to fill in are:
* `dirname`: the directory in which your image can be found
* `filename`: the name of your image file (without its extension)
* `image_format`: the extension of your image file (`tif` or `czi`)
* `channel_names`: the list of names given to each channel of the image
* `membrane_channel`: the name of the channel correspondinng to the cell wall/membrane marker
* `microscope_orientation`: whether the image comes from an upright (`1`) or inverted (`-1`) microscope

Next, if you wish to perform an automatic segmentation of the membrane image channel, you can set parameters for an auto-seeded 3D watershed segmentation:
* `gaussian_sigma`: the size of the Gaussian kernel used for seed detection
* `h_min`: the minimal signal intensity height to separate 2 seeds
* `segmentation_gaussian_sigma`: the size of the Gaussian kernel used for watershed
* `volume_threshold`: the maximal cell volume (in µm3) above which cells will be suppressed

**Note**: The value of the `h_min` parameter must be set differently for an *8-bit* image and for a *16-bit* image.

**Note**: Instead of using the automatic segmentation procedure, you can provide a segmented image coming from the tool of your choice as a 3D `.tif` image. To do so, you should create in the same directory as your image, a directory named `{filename}/`, and save the segmented image there as `{filename}_{membrane_channel}_seg.tif`. If you do so, **you must set the `recompute_segmentation` parameter to `False`** otherwise your segmented image file will be **overwritten**!

### Run the notebook

----

Once you have configured the notebook, you can navigate in the menu to `Kernel > Restart & Run All` to execute all the cells in the notebook.

This will save various files in the `{filename}/` directory:
* The segmented image as `{filename}_{membrane_channel}_seg.tif`
* Screenshots of the successive steps of the analysis pipeline
* A `.csv` data-sheet containing information for each cell in the tissue
* A `.csv` data-sheet containing the average layer heights

